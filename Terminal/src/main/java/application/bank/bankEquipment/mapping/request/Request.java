package application.bank.bankEquipment.mapping.request;

import application.bank.bankEquipment.server.Server;
import application.bank.bankEquipment.terminal.Terminal;
import application.bank.card.Card;
import application.user.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Request {

    private User user;
    private List<User> usersTransactions;
    private String cardNumber;
    private String pinCode;
    private BigDecimal moneyForCardCreating;
    private BigDecimal moneyForTransfer;
    private Card userTransactionFromCard;
    private Card userTransactionToCard;
    private final Terminal terminal;
    private final Server server;

    public Request(Terminal terminal, Server server) {
        this.terminal = terminal;
        this.server = server;
    }


    public void setUser(User user) {
        this.user = user;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public void setMoneyForCardCreating(BigDecimal moneyForCardCreating) {
        this.moneyForCardCreating = moneyForCardCreating;
    }

    public List<User> getUsersTransactions() {
        return usersTransactions;
    }

    public void setUsersTransactions(User... users) {
        this.usersTransactions = new ArrayList<>();
        this.usersTransactions.addAll(Arrays.asList(users));
    }

    public void setMoneyForTransfer(BigDecimal moneyForTransfer) {
        this.moneyForTransfer = moneyForTransfer;
    }

    public User getUser() {
        return user;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getPinCode() {
        return pinCode;
    }

    public BigDecimal getMoneyForCardCreating() {
        return moneyForCardCreating;
    }

    public BigDecimal getMoneyForTransfer() {
        return moneyForTransfer;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public Server getServer() {
        return server;
    }

    public Card getUserTransactionFromCard() {
        return userTransactionFromCard;
    }

    public void setUserTransactionFromCard(Card userTransactionFromCard) {
        this.userTransactionFromCard = userTransactionFromCard;
    }

    public Card getUserTransactionToCard() {
        return userTransactionToCard;
    }

    public void setUserTransactionToCard(Card userTransactionToCard) {
        this.userTransactionToCard = userTransactionToCard;
    }
}
