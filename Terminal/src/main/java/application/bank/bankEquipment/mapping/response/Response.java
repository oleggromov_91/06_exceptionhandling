package application.bank.bankEquipment.mapping.response;

import java.util.List;

public class Response {

 private final HttpStatusCode httpStatusCode;
 private final List<String> body;

    public Response(HttpStatusCode httpStatusCode, List<String> body) {
        this.httpStatusCode = httpStatusCode;
        this.body = body;
    }

    public HttpStatusCode getHttpStatusCode() {
        return httpStatusCode;
    }

    public List<String> getBody() {
        return body;
    }
}
