package application.bank.bankEquipment.terminal;

import application.bank.bankEquipment.mapping.request.Request;
import application.bank.bankEquipment.server.Server;
import application.bank.card.Card;
import application.user.User;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class SberTerminal implements Terminal {

    private final UUID id = UUID.randomUUID();
    boolean isUserValidate = false;
    private final Server server;

    public SberTerminal(Server server) {
        this.server = server;
    }

    @Override
    public Request validateUser(User cardHolder, String pinCode, String cardNumber) {
        firstCheckPinInput(pinCode);
        server.nonNullPredicateForRequest(cardHolder, pinCode, cardNumber, cardNumber);
        Request validate = new Request(this, server);
        validate.setUser(cardHolder);
        validate.setPinCode(pinCode);
        validate.setCardNumber(cardNumber);
        server.userValidation(validate);
        return validate;
    }

    @Override
    public Request createNewCard(User cardHolder, String pinCode, String cardNumber) {
        server.nonNullPredicateForRequest(cardHolder, pinCode, cardNumber, cardNumber);
        Request createCardWithoutMoney = new Request(this, server);
        createCardWithoutMoney.setUser(cardHolder);
        createCardWithoutMoney.setPinCode(pinCode);
        createCardWithoutMoney.setCardNumber(cardNumber);
        return createCardWithoutMoney;
    }

    @Override
    public Request createNewCardWithMoney(User cardHolder, String pinCode, String cardNumber, BigDecimal money) {
        server.nonNullPredicateForRequest(cardHolder, pinCode, cardNumber, money);
        Request createCardWithMoney = new Request(this, server);
        createCardWithMoney.setUser(cardHolder);
        createCardWithMoney.setPinCode(pinCode);
        createCardWithMoney.setCardNumber(cardNumber);
        createCardWithMoney.setMoneyForCardCreating(money);
        return createCardWithMoney;
    }

    @Override
    public Request transferMoneyFromUserToUser(String userFromPinCode, String userFromCardNumber, User from, User to, String userToCardNumber, BigDecimal moneyAmount) {
        server.nonNullPredicateForRequest(userFromPinCode,userFromCardNumber,from,to, moneyAmount);
        validateUser(from, userFromPinCode, userFromCardNumber);
        Request moneyTransfer = new Request(this, server);
        moneyTransfer.setUsersTransactions(from, to);
        moneyTransfer.setUserTransactionFromCard(from.takeCard(userFromCardNumber));
        moneyTransfer.setUserTransactionToCard(to.takeCard(userToCardNumber));
        moneyTransfer.setMoneyForTransfer(moneyAmount);
        return moneyTransfer;
    }

    @Override
    public Request transferMoneyAccountToAccountOfUser(String cardHolderPinCode, User cardHolder, BigDecimal moneyAmount, String cardNumberFrom, String cardNumberTo) {
        server.nonNullPredicateForRequest(cardHolderPinCode, cardHolder, moneyAmount, cardNumberFrom, cardNumberTo);
        validateUser(cardHolder, cardHolderPinCode, cardNumberFrom);
        Request moneyTransfer = new Request(this, server);
        moneyTransfer.setUser(cardHolder);
        moneyTransfer.setMoneyForTransfer(moneyAmount);
        moneyTransfer.setUserTransactionFromCard(cardHolder.takeCard(cardNumberFrom));
        moneyTransfer.setUserTransactionToCard(cardHolder.takeCard(cardNumberTo));
        return moneyTransfer;
    }

    private void firstCheckPinInput(String input) {
        String VALIDATOR = "\\d \\d \\d \\d";
        if (Objects.isNull(input) |
                input.isBlank() |
                !input.matches(VALIDATOR)) throw new IllegalArgumentException("pin code is not valid");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SberTerminal that = (SberTerminal) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(server, that.server);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, server);
    }
}
