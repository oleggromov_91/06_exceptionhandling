package application.bank.bankEquipment.terminal;

import application.bank.card.Card;
import application.user.User;
import application.bank.bankEquipment.mapping.request.Request;

import java.math.BigDecimal;

public interface Terminal {

    Request validateUser(User cardHolder, String pinCode, String cardNumber);

    Request createNewCard(User cardHolder, String pinCode, String cardNumber);

    Request createNewCardWithMoney(User cardHolder, String pinCode, String cardNumber, BigDecimal money);

    Request transferMoneyFromUserToUser (String userFromPinCode, String userFromCardNumber, User from, User to, String userToCardNumber, BigDecimal moneyAmount);

    Request transferMoneyAccountToAccountOfUser (String cardHolderPinCode, User cardHolder, BigDecimal moneyAmount, String cardNumberFrom, String CardNumberTo);

}
