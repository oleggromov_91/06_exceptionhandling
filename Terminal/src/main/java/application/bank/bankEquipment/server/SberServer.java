package application.bank.bankEquipment.server;

import application.bank.bankEquipment.mapping.request.Request;
import application.bank.card.Card;
import application.bank.exceptions.IllegalOperationException;
import application.user.User;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class SberServer implements Server {

    private final static ServerDB serverDB = new ServerDB();
    private final static Transaction transactionService = new Transaction();

    @Override
    public boolean userValidation(Request request) {
        serverDB.updateDB(request);
        PinValidator pinValidator = new PinValidator();
        try {
            Method validate = pinValidator.getClass().getDeclaredMethod("validateUser", Request.class);
            validate.setAccessible(true);
            return (Boolean) validate.invoke(pinValidator, request);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean createNewCard(Request request) {
        serverDB.updateDB(request);
        if (nonNullPredicateForRequest(request.getUser(), request.getCardNumber(), request.getPinCode())) {
            User cardHolder = request.getUser();
            String cardNumber = request.getCardNumber();
            String pinCode = request.getPinCode();

            Card card = new Card(pinCode, cardNumber, cardHolder.hashCode());
            if (cardHolder.getCards().containsValue(card)) {
                throw new IllegalOperationException("card exist");
            } else {
                cardHolder.getCards().put(cardNumber, card);
                System.out.println("Card created");
            }
        }
        return false;
    }

    @Override
    public boolean transferMoneyFromUserToUser(Request request) {
        serverDB.updateDB(request);
        User from = request.getUsersTransactions().get(0);
        Card cardUserFrom = request.getUserTransactionFromCard();
        User to = request.getUsersTransactions().get(1);
        Card cardUserTo = request.getUserTransactionToCard();
        BigDecimal money = request.getMoneyForTransfer();
        Map<Boolean, List<Card>> transactionResult = createTransaction(cardUserFrom, cardUserTo, money);

        for (int cardIndex = 0; cardIndex < transactionResult.keySet().size(); cardIndex++) {
            List<Card> values = transactionResult.get(true);
            from.getCards().replace(cardUserFrom.getCardNumber(), values.get(cardIndex));
            to.getCards().replace(cardUserTo.getCardNumber(), values.get(cardIndex + 1));
        }

        return true;
    }

    @Override
    public boolean transferMoneyAccountToAccountOfUser(Request request) {
//        String cardHolderPinCode, User cardHolder, BigDecimal moneyAmount


        return false;
    }

    @Override
    public boolean createNewCardWithMoney(Request request) {
        serverDB.updateDB(request);
        if (nonNullPredicateForRequest(request.getUser(), request.getCardNumber(), request.getPinCode(), request.getMoneyForCardCreating())) {
            User cardHolder = request.getUser();
            String cardNumber = request.getCardNumber();
            String pinCode = request.getPinCode();
            BigDecimal money = request.getMoneyForCardCreating();
            Card card = new Card(pinCode, money, cardNumber, cardHolder.hashCode());

            if (cardHolder.getCards().containsValue(card)) {
                throw new IllegalOperationException("card exist");
            } else {
                cardHolder.getCards().put(cardNumber, card);
                System.out.println("Card created");
            }
        }
        return false;
    }

        private Map<Boolean, List<Card>> createTransaction(Card from, Card to, BigDecimal money)
        {
            Map<Boolean, List<Card>> transactionResult = transactionService.transferMoney(from, to, money);

            if (transactionResult.keySet().stream().allMatch(result -> result.equals(true)))
            {
                return transactionResult;
            }
            throw new IllegalOperationException("Operation was rejected");
        }




}
