package application.bank.bankEquipment.server;

import application.bank.bankEquipment.mapping.request.Request;
import application.bank.exceptions.BrokenRequestParamException;
import application.user.User;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;

public interface Server {

    int TRANSACTION_CONDITION = 100;

    boolean userValidation(Request request);

    boolean createNewCard(Request request);

    boolean createNewCardWithMoney(Request request);

    boolean transferMoneyFromUserToUser(Request request);

    boolean transferMoneyAccountToAccountOfUser(Request request);

    default boolean nonNullPredicateForRequest(Object... args) {
        if(Arrays.stream(args).allMatch(Objects::nonNull))
        {
           return true;
        }
        throw new BrokenRequestParamException("Request is broken");
    }

    default boolean nonNullPredicateForValidate(User cardHolder, String pinCode, String cardNumber) {
        if(Objects.isNull(cardHolder) & Objects.isNull(pinCode) & Objects.isNull(cardNumber) )
        {
            throw new BrokenRequestParamException("Request is broken");
        }
        return false;
    }

}
