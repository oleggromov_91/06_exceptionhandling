package application.bank.bankEquipment.server;

import application.bank.card.Card;
import application.bank.exceptions.IllegalOperationException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Transaction {
    private Map<Boolean, List<Card>> cardsOperationResult = new HashMap<>();

    /**
     * @param from  card of cardholder who send the money
     * @param to    card of cardholder who receive the money
     * @param money money for operation
     *              It clone cards, and perform the operation of withdrawal and crediting.
     *              If no exceptions occurred during execution, will return clones,
     *              if there were, will return unchanged.
     * @return true - success operation, false - operation has error
     */
    protected Map<Boolean, List<Card>> transferMoney(Card from, Card to, BigDecimal money) {
        isTransactionConditionRequired(money);
        clearResult();
        Card cardFromClone = (Card) bankObjectsCloner(from);
        Card cardToClone = (Card) bankObjectsCloner(to);

        try {
            Field cardFromField = cardFromClone.getClass().getDeclaredField("moneyAmount");
            Field cardToField = cardToClone.getClass().getDeclaredField("moneyAmount");
            cardFromField.setAccessible(true);
            cardToField.setAccessible(true);
            BigDecimal cardMoneyAmountFrom = (BigDecimal) cardFromField.get(cardFromClone);
            BigDecimal cardMoneyAmountTo = (BigDecimal) cardFromField.get(cardToClone);
            cardFromField.set(cardFromClone, cardMoneyAmountFrom.subtract(money));
            cardToField.set(cardToClone, cardMoneyAmountTo.add(money));
            this.cardsOperationResult.put(true, List.of(cardFromClone, cardToClone));
            return this.cardsOperationResult;
        } catch (Exception e) {
            clearResult();
            this.cardsOperationResult.put(false, List.of(from, to));
            return this.cardsOperationResult;
        }
    }

    /**
     * @param object which should will clone
     * @return clones
     */
    private Object bankObjectsCloner(Object object) {
        try {
            Method cloneMethod = object.getClass().getDeclaredMethod("clone");
            cloneMethod.setAccessible(true);
            return cloneMethod.invoke(object);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        throw new IllegalOperationException("Illegal operation");
    }

    /**
     * @param money for operation
     * @return if money match of TRANSACTION_CONDITION
     */
    private boolean isTransactionConditionRequired(BigDecimal money) {
        if (money.doubleValue() % Server.TRANSACTION_CONDITION == 0) {
            return true;
        }
        throw new IllegalOperationException("money amount should be multiplicity by " + Server.TRANSACTION_CONDITION);
    }

    private void clearResult() {
        this.cardsOperationResult.clear();
    }


}
