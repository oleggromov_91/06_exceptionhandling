package application.bank.bankEquipment.server;


import application.bank.bankEquipment.mapping.request.Request;
import application.bank.card.Card;
import application.bank.exceptions.AccountIsLockedException;
import application.bank.exceptions.WrongPinCodeException;
import application.user.User;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class PinValidator {

    protected boolean validateUser(final Request request) {
        try {
            checkPin(request);
            return true;
        } catch (WrongPinCodeException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void checkPin(final Request request) {
        User cardHolder = request.getUser();
        String cardNumber = request.getCardNumber();
        String pinCode = request.getPinCode();
        Card card = cardHolder.takeCard(cardNumber);

        try {
            Field pinCodeField = card.getClass().getDeclaredField("pinCode");
            pinCodeField.setAccessible(true);
            Object pinCodeClass = pinCodeField.get(card);
            Method method = pinCodeClass.getClass().getDeclaredMethod("decrypting", String.class);
            method.setAccessible(true);
            boolean isValidPin = (Boolean) method.invoke(pinCodeClass, pinCode);
            if (!isValidPin) {
                lock(cardHolder, cardNumber);
                throw new WrongPinCodeException("Wrong pin code");
            } else {
                request.getUser().setOnline(true);
                System.out.println("User successfully approved");
            }
        } catch (NoSuchFieldException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void lock(final User cardHolder, final String cardNumber) {
        Card card = cardHolder.takeCard(cardNumber);
        long lockTime = 6000;
        try {
            Field codeInputTry = Card.class.getDeclaredField("codeInputTry");
            codeInputTry.setAccessible(true);
            try {
                if ((Integer) codeInputTry.get(card) == 0) {
                    throw new AccountIsLockedException("Your account is locked for " + lockTime / 1000 + " seconds");
                } else {
                    int temp = (Integer) codeInputTry.get(card);
                    codeInputTry.set(card, --temp);
                }

            } catch (AccountIsLockedException e) {
                e.printStackTrace();
                sleep(lockTime);
                codeInputTry.setAccessible(true);
                codeInputTry.set(card, 3);
                System.out.println("Your account is unlocked");
            }

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void sleep(final long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
