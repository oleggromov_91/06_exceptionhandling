package application.bank.bankEquipment.server;

import application.bank.bankEquipment.mapping.request.Request;
import application.bank.bankEquipment.terminal.Terminal;
import application.user.User;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

class ServerDB {

    private final Set<User> users = new HashSet<>();
    private final Set<Terminal> terminals = new HashSet<>();


    protected void updateDB(Request request) {
        addTerminal(request);
        addUser(request);
    }

    private void addTerminal(Request request) {
        if (Objects.nonNull(request.getTerminal())) {
            terminals.add(request.getTerminal());
        }
    }

    private void addUser(Request request) {
        if (Objects.nonNull(request.getUser())) {
            users.add(request.getUser());
        }
    }

}
