package application.bank.card;

import java.math.BigDecimal;
import java.util.Objects;

public class Card implements Cloneable{

    private Encrypting pinCode;
    private final int cardHolderHash;
    private BigDecimal moneyAmount = new BigDecimal(0);
    private final String cardNumber;
    private int codeInputTry = 3;

    public Card(String pinCode, String cardNumber, int cardHolderHash) {
        this.pinCode = new Encrypting(pinCode);
        this.cardHolderHash = cardHolderHash;
        this.cardNumber = validateCardNumber(cardNumber);
    }

    public Card(String pinCode, BigDecimal moneyAmount, String cardNumber, int cardHolderHash) {
        this.pinCode = new Encrypting(pinCode);
        this.moneyAmount = moneyAmount;
        this.cardHolderHash = cardHolderHash;
        this.cardNumber = validateCardNumber(cardNumber);
    }

    private String validateCardNumber(String input)
    {
        String VALIDATOR = "\\d{4} \\d{4} \\d{4} \\d{4}";
        if(input.matches(VALIDATOR)) return input;
        throw new IllegalArgumentException("Card number is not valid");
    }


    public BigDecimal showBalance()
    {
        return this.moneyAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return cardHolderHash == card.cardHolderHash &&
                Objects.equals(cardNumber, card.cardNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardHolderHash, cardNumber);
    }

    public String getCardNumber() {
        return cardNumber;
    }

    @Override
   protected Object clone() throws CloneNotSupportedException {
        Card clone = (Card) super.clone();
        clone.pinCode = this.pinCode;
        clone.moneyAmount = this.moneyAmount;
        return clone;
    }

    public void setPinCode(Encrypting pinCode) {
        this.pinCode = pinCode;
    }

}
