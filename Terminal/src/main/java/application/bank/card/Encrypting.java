package application.bank.card;

import java.util.Arrays;

class Encrypting {

    private final byte[] pinCode;
    private int codeInputTry = 3;

    public Encrypting(String pinCode) {
        this.pinCode = encrypt(pinCode);
    }

    private boolean decrypting(String pinCode) {
        byte[] checkInput = encrypt(pinCode);
        return (Arrays.equals(checkInput, this.pinCode));
    }

    private byte[] encrypt(String input) {
        byte[] code = input.getBytes();
        for (int i = 0; i < code.length; i++) {
            code[i] = (byte) (code[i] << 1);
        }
        return code;
    }

    private String decrypt(byte[] code) {
        for (int i = 0; i < code.length; i++) {
            code[i] = (byte) (code[i] >> 1);
        }
        return new String(code);
    }

}
