package application.bank.exceptions;

public class BrokenRequestParamException extends Error {

    public BrokenRequestParamException() {
        super();
    }

    public BrokenRequestParamException(String message) {
        super(message);
    }

    public BrokenRequestParamException(String message, Throwable cause) {
        super(message, cause);
    }
}
