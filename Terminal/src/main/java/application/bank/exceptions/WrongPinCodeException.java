package application.bank.exceptions;

public class WrongPinCodeException extends Error {

    public WrongPinCodeException() {
        super();
    }

    public WrongPinCodeException(String message) {
        super(message);
    }

    public WrongPinCodeException(String message, Throwable cause) {
        super(message, cause);
    }
}
