package application.user;

import application.bank.card.Card;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class User implements Cloneable {

    private Map<String, Card> cards = new HashMap<>();
    private final String firstName;
    private final String secondName;
    private final String middleName;
    private final UUID id = UUID.randomUUID();
    private boolean isOnline = false;


    public User(String firstName, String secondName, String middleName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
    }


    public Card takeCard(String cardNumber) {
        return cards.get(cardNumber);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return
                Objects.equals(firstName, user.firstName) &&
                        Objects.equals(secondName, user.secondName) &&
                        Objects.equals(middleName, user.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, secondName, middleName);
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public Map<String, Card> getCards() {
        return cards;
    }

}
